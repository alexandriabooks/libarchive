import typing
import io
import zipfile

import libarchive
import libarchive.read

from alexandriabooks_core import accessors, model
from alexandriabooks_core.services import BookParserService, registry
from alexandriabooks_formats.archive import detect_source_type_from_archive_contents
from alexandriabooks_formats.epub import parse_epub_metadata
from alexandriabooks_formats.imagebook import parse_imagebook_metadata
from alexandriabooks_formats.audiobook import parse_audio_book_metadata


async def _get_source_type_from_accessor(accessor):
    filename = accessor.get_filename().lower()

    source_type = None
    if filename.endswith(".zip"):
        source_type = await detect_source_type_from_archive_contents(
            LibArchiveBookAccessor(accessor),
            "ZIP"
        )
    elif (
        filename.endswith(".tar") or
        filename.endswith(".tgz") or
        filename.endswith(".tar.gz") or
        filename.endswith(".tar.bz2")
    ):
        source_type = await detect_source_type_from_archive_contents(
            LibArchiveBookAccessor(accessor),
            "TAR"
        )
    elif filename.endswith(".rar"):
        source_type = await detect_source_type_from_archive_contents(
            LibArchiveBookAccessor(accessor),
            "RAR"
        )
    elif filename.endswith(".7za"):
        source_type = await detect_source_type_from_archive_contents(
            LibArchiveBookAccessor(accessor),
            "SEVENZIP"
        )
    elif filename.endswith(".ace"):
        source_type = await detect_source_type_from_archive_contents(
            LibArchiveBookAccessor(accessor),
            "ACE"
        )
    else:
        source_type = model.SourceType.find_source_type(filename)

    return source_type


class LibArchiveBookAccessor(accessors.ArchiveBookAccessor):
    def __init__(self, accessor):
        super(LibArchiveBookAccessor, self).__init__(accessor)

    async def _get_libarchive_reader(self):
        local_path = self.accessor.get_local_path()
        if local_path:
            return libarchive.file_reader(local_path)
        return libarchive.stream_reader(await self.accessor.get_stream())

    async def get_archive_comment(self):
        filename = self.accessor.get_filename().lower()
        if filename.endswith(".zip") or filename.endswith(".cbz"):
            with await self.accessor.get_stream() as stream:
                with zipfile.ZipFile(stream, "r") as zip_handle:
                    return zip_handle.comment
        return None

    async def get_file_listing(self):
        with await self._get_libarchive_reader() as archive:
            return [entry.pathname for entry in archive]

    async def get_file_stream(self, path):
        with await self._get_libarchive_reader() as archive:
            for entry in archive:
                if (
                    entry.isfile and
                    entry.pathname.replace("\\", "/") == path.replace("\\", "/")
                ):
                    stream = io.BytesIO()
                    for block in entry.get_blocks():
                        stream.write(block)
                    stream.seek(0)
                    return stream

        return None

    async def get_file_streams(self):
        with await self._get_libarchive_reader() as archive:
            for entry in archive:
                if entry.isfile:
                    stream = io.BytesIO()
                    for block in entry.get_blocks():
                        stream.write(block)
                    stream.seek(0)
                    yield (entry.pathname, stream)


class LibArchiveBookParserService(BookParserService):
    def __init__(self, service_registry: registry.ServiceRegistry, **kwargs):
        self.service_registry: registry.ServiceRegistry = service_registry

    def __str__(self):
        return "EPUB/CBZ/CBT/CBR/CB7/CBA Book Parser (libarchive)"

    async def can_parse(self, accessor):
        source_type = await _get_source_type_from_accessor(accessor)
        if source_type is None:
            return False
        return (
            source_type.name.startswith("ZIP_") or
            source_type in [
                model.SourceType.EPUB,
                model.SourceType.CBZ,
                model.SourceType.CBT,
                model.SourceType.CBR,
                model.SourceType.CB7,
                model.SourceType.CBA,
            ]
        )

    async def parse_book(self, accessor):
        source_type = await _get_source_type_from_accessor(accessor)

        if source_type is None:
            return None

        metadata = {"source_type": source_type}

        if source_type == model.SourceType.EPUB:
            metadata.update(await parse_epub_metadata(
                LibArchiveBookAccessor(accessor),
                self.service_registry
            ))
        elif source_type in [
            model.SourceType.CBZ,
            model.SourceType.CBT,
            model.SourceType.CBR,
            model.SourceType.CB7,
            model.SourceType.CBA,
        ]:
            metadata.update(await parse_imagebook_metadata(
                LibArchiveBookAccessor(accessor),
                self.service_registry
            ))
        elif source_type in [
            model.SourceType.ZIP_MP3,
            model.SourceType.ZIP_FLAC,
            model.SourceType.ZIP_M4A,
            model.SourceType.ZIP_OGG,
            model.SourceType.ZIP_WMA,
            model.SourceType.ZIP_WAV
        ]:
            metadata.update(await parse_audio_book_metadata(
                LibArchiveBookAccessor(accessor),
                self.service_registry
            ))
        else:
            return None

        return metadata

    async def enhance_accessor(
        self,
        accessor: model.FileAccessor
    ) -> typing.List[model.FileAccessor]:
        """
        Attempts to find a more specific accessor to aid processing the book.
        :param accessor: The accessor to read the book.
        :return: A list of enhanced accessors.
        """
        if self.can_parse(accessor):
            return [LibArchiveBookAccessor(accessor)]
        return []
