import logging
import typing

logger = logging.getLogger(__name__)

ALEXANDRIA_PLUGINS: typing.Dict[str, typing.Any] = {}
try:
    from .booktypes import LibArchiveBookParserService
    from .transcoder import LibArchiveBookTranscoderService

    ALEXANDRIA_PLUGINS["BookParserService"] = [LibArchiveBookParserService]
    ALEXANDRIA_PLUGINS["BookTranscoderService"] = [LibArchiveBookTranscoderService]
except (ImportError, TypeError) as e:
    logger.debug("Not loading libarchive plugin", exc_info=e)
