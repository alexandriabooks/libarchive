import typing
import logging
import os.path
import tempfile
import shutil
import io

import libarchive

from alexandriabooks_core import model, accessors
from alexandriabooks_core.utils import copy_streams
from alexandriabooks_core.services import ServiceRegistry, BookTranscoderService
from alexandriabooks_formats import imagebook

logger = logging.getLogger(__name__)


class LibArchiveBookTranscoderService(BookTranscoderService):
    def __init__(self, **kwargs):
        super().__init__()
        self._service_registry: ServiceRegistry = kwargs["service_registry"]

    def __str__(self):
        return "CBZ/CBT/CBR/CB7/CBA to CBZ/CBT/CB7 Book Transcoder (libarchive)"

    async def get_book_transcode_targets(self, src):
        if src in [
            model.SourceType.CB7,
            model.SourceType.CBA,
            model.SourceType.CBR,
            model.SourceType.CBT,
            model.SourceType.CBZ,
            model.SourceType.PDF,
        ]:
            return [model.SourceType.CBZ, model.SourceType.CBT, model.SourceType.CB7]
        if src.name.startswith("ZIP_"):
            return [
                source_type
                for source_type in model.SourceType
                if (
                    source_type.name.startswith("ZIP_") or
                    source_type.name.startswith("TAR_") or
                    source_type.name.startswith("SEVENZIP_")
                )
            ]
        return []

    async def _transcode_book(
        self,
        book_source: model.SourceProvidedBookMetadata,
        accessor: model.FileAccessor,
        desired_type: model.SourceType,
        image_transcoder_options: typing.Optional[typing.Dict[str, typing.Any]] = None,
        audio_transcoder_options: typing.Optional[typing.Dict[str, typing.Any]] = None,
    ):
        def iter_stream_chunks(stream):
            while True:
                data = stream.read(4096)
                if data:
                    yield data
                else:
                    break

        if not hasattr(accessor, "get_file_streams"):
            enhanced_accessors = await self._service_registry.enhance_accessor(accessor)
            for enhanced_accessor in enhanced_accessors:
                if hasattr(enhanced_accessor, "get_file_streams"):
                    accessor = enhanced_accessor
                    break
            else:
                logger.warning("Got passed a non ArchiveBookAccessor.")
                return None

        archive_accessor: accessors.ArchiveBookAccessor = typing.cast(
            accessors.ArchiveBookAccessor,
            accessor
        )

        logger.info("Transcoding to %s", desired_type)
        if (
            desired_type == model.SourceType.CBZ or
            desired_type.name.startswith("ZIP_")
        ):
            format_name = "zip"
            filter_name = None
        elif (
            desired_type == model.SourceType.CBT or
            desired_type.name.startswith("TAR_")
        ):
            format_name = "gnutar"
            filter_name = None
        elif (
            desired_type == model.SourceType.CB7 or
            desired_type.name.startswith("SEVENZIP_")
        ):
            format_name = "7zip"
            filter_name = None
        else:
            return None

        with archive_accessor:
            dest = tempfile.NamedTemporaryFile().__enter__()

            dest_archive: libarchive.write.ArchiveWrite
            with libarchive.fd_writer(
                dest.fileno(), format_name, filter_name
            ) as dest_archive:
                wrote_comic_rack_metadata = False
                for filename, src_stream in await archive_accessor.get_file_streams():
                    temp_parent = os.path.dirname(filename)
                    if filename.lower().endswith("comicinfo.xml"):
                        wrote_comic_rack_metadata = True

                    if temp_parent and not os.path.isdir(temp_parent):
                        os.makedirs(temp_parent)

                    with src_stream:
                        if os.path.basename(filename).startswith("."):
                            continue

                        dest_filename = filename
                        with io.BytesIO() as dest_stream:
                            if (
                                model.ImageType.find_image_type(filename) and
                                image_transcoder_options
                            ):
                                logger.debug(
                                    "Transcoding image %s with options %s",
                                    filename,
                                    image_transcoder_options,
                                )

                                await copy_streams(
                                    await self._service_registry.transcode_image(
                                        src_stream, **image_transcoder_options
                                    ),
                                    dest_stream
                                )

                                dest_type = image_transcoder_options.get("desired_type")
                                if dest_type:
                                    dest_filename = (
                                        os.path.splitext(filename)[0] +
                                        "." +
                                        dest_type.ext()
                                    )
                            elif (
                                model.AudioType.find_audio_type(filename) and
                                audio_transcoder_options
                            ):
                                logger.debug(
                                    "Transcoding audio %s with options %s",
                                    filename,
                                    audio_transcoder_options,
                                )

                                await copy_streams(
                                    await self._service_registry.transcode_audio(
                                        src_stream, **audio_transcoder_options
                                    ),
                                    dest_stream
                                )

                                dest_type = audio_transcoder_options.get("desired_type")
                                if dest_type:
                                    dest_filename = (
                                        os.path.splitext(filename)[0] +
                                        "." +
                                        dest_type.ext()
                                    )
                            else:
                                logger.debug("Copying %s", filename)
                                shutil.copyfileobj(src_stream, dest_stream)

                            size = dest_stream.tell()
                            dest_stream.seek(0)

                            logging.debug("Adding to temp file... size = %d", size)
                            dest_archive.add_file_from_memory(
                                dest_filename, size, iter_stream_chunks(dest_stream)
                            )

                if not wrote_comic_rack_metadata:
                    comic_rack_metadata = (await imagebook.create_comic_rack_metadata(
                        self._service_registry,
                        book_source
                    )).encode("utf8")
                    dest_archive.add_file_from_memory(
                        "ComicInfo.xml",
                        len(comic_rack_metadata),
                        comic_rack_metadata
                    )

            logging.debug("Archive size = %d", dest.tell())
            dest.seek(0)
            return dest

    async def transcode_book(
        self,
        book_source: model.SourceProvidedBookMetadata,
        accessor: model.FileAccessor,
        desired_type: model.SourceType,
        image_transcoder_options: typing.Optional[typing.Dict[str, typing.Any]] = None,
        audio_transcoder_options: typing.Optional[typing.Dict[str, typing.Any]] = None,
        **kwargs
    ):
        image_transcoder_options = image_transcoder_options or {}
        audio_transcoder_options = audio_transcoder_options or {}
        if (
            desired_type in [
                model.SourceType.CBZ,
                model.SourceType.CBT,
                model.SourceType.CB7
            ] or
            desired_type.name.startswith("ZIP_") or
            desired_type.name.startswith("TAR_") or
            desired_type.name.startswith("SEVENZIP_")
        ):
            if (
                desired_type.name.startswith("ZIP_") or
                desired_type.name.startswith("TAR_") or
                desired_type.name.startswith("SEVENZIP_")
            ):
                pos = desired_type.name.find("_") + 1
                audio_transcoder_options = audio_transcoder_options.copy()
                audio_transcoder_options["desired_type"] = model.AudioType.to_audio_type(
                    desired_type.name[pos:]
                )
            return await self._transcode_book(
                book_source,
                accessor,
                desired_type,
                image_transcoder_options,
                audio_transcoder_options
            )
        return None
